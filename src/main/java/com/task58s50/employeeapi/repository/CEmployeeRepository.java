package com.task58s50.employeeapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task58s50.employeeapi.models.CEmployee;

public interface CEmployeeRepository extends JpaRepository <CEmployee, Long>{
    
}
