package com.task58s50.employeeapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task58s50.employeeapi.models.CEmployee;
import com.task58s50.employeeapi.repository.CEmployeeRepository;

@RestController
@CrossOrigin
@RequestMapping
public class CEmployeeController {
    @Autowired
    CEmployeeRepository employeeRepository;
    @GetMapping("employees")
    public ResponseEntity <List<CEmployee>> getAllEmployee(){
        try {
            List<CEmployee> listEmployee = new ArrayList<CEmployee>();
            employeeRepository.findAll().forEach(listEmployee :: add);
            return new ResponseEntity<>(listEmployee, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
